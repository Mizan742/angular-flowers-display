import {Component, OnInit, Input} from '@angular/core';
import {Flower} from '../flower';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import {FlowerService} from '../flower.service';

@Component({
  selector: 'app-flower-detail',
  templateUrl: './flower-detail.component.html',
  styleUrls: ['./flower-detail.component.css'],
})
export class FlowerDetailComponent implements OnInit {
  @Input()
  flower: Flower; //component communication parent -> children  using import decorator

  constructor(
    private route: ActivatedRoute, //The ActivatedRoute holds information about the route to this instance of the FlowerDetailComponent.
    private flowerService: FlowerService,
    private location: Location //The location is an Angular service for interacting with the browser. You'll use it later to navigate back to the view that navigated here.
  ) {}

  ngOnInit(): void {
    this.getFlower();
  }

  getFlower(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.flowerService
      .getFlower(id)
      .subscribe(flower => (this.flower = flower));
  }

  save(): void {
    this.flowerService.updateFlower(this.flower).subscribe(() => this.goBack());
  }

  goBack(): void {
    this.location.back();
  }
}
