import {Component, OnInit} from '@angular/core';
import {FlowerMessageService} from '../flower-message.service';

@Component({
  selector: 'app-flower-messages',
  templateUrl: './flower-messages.component.html',
  styleUrls: ['./flower-messages.component.css'],
})
export class FlowerMessagesComponent implements OnInit {
  constructor(public flowerMessageService: FlowerMessageService) {}

  ngOnInit() {}
}
