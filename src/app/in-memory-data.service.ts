import {InMemoryDbService} from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const flowers = [
      {
        id: 1,
        name: 'Dahlia',
        url:
          'https://www.almanac.com/sites/default/files/styles/primary_image_in_article/public/image_nodes/dahlias-red-white.jpg?itok=PbdHFu7i',
      },
      {
        id: 2,
        name: 'Lily',
        url:
          'https://www.gardendesign.com/pictures/images/675x529Max/site_3/asiatic-lily-cappuccino-lily-creative-commons_11653.jpg',
      },
      {
        id: 3,
        name: 'Zinnia',
        url:
          'https://savvygardening.com/wp-content/uploads/2015/08/zinniaFlower.jpg',
      },
      {
        id: 4,
        name: 'Carnation',
        url:
          'https://www.fiftyflowers.com/site_files/FiftyFlowers/Image/Product/Cheerio-Carnation-Close-500_93a91b1c.jpg',
      },
      {
        id: 5,
        name: 'Jasmine',
        url:
          'http://www.herbco.com/images/Category/large/c-327-jasmine-flower.jpg',
      },
      {
        id: 6,
        name: 'Poppy',
        url:
          'https://cdn.shopify.com/s/files/1/1003/1822/products/AdobeStock_110918523.jpeg?v=1479880659',
      },
      {
        id: 7,
        name: 'Chrysanthemum',
        url:
          'https://pixfeeds.com/images/flowers/chrysanthemums/1280-535194346-orange-and-pink-chrysanthemum.jpg',
      },
      {
        id: 8,
        name: 'Daffodil',
        url:
          'https://cdn8.bigcommerce.com/s-ynwe6/images/stencil/1024x1024/products/146/349/daffodil__71031.1451979113.jpg?c=2',
      },
      {
        id: 9,
        name: 'Peony',
        url:
          'https://www.auntyflo.com/sites/default/files/styles/contentpage/public/Peony.jpg?itok=T-NvgN4Z',
      },
      {
        id: 10,
        name: 'Anemone',
        url:
          'https://www.reef2reef.com/attachments/rock-anemone-1-586-jpg.127392/',
      },
      {
        id: 11,
        name: 'Rose',
        url:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQehq4ZCIFLjOe9jeo1w626PpQxelbL8BmfYUJq2MFif9SvAAQgmA',
      },
    ];
    return {flowers};
  }
}
