import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {Flower} from './flower';
import {FlowerMessageService} from './flower-message.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, map, tap} from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'}),
};

@Injectable({
  providedIn: 'root',
})
export class FlowerService {
  private flowersUrl = 'api/flowers'; // URL to web api
  constructor(
    private http: HttpClient,
    private flowerMessageService: FlowerMessageService
  ) {}

  /*  getFlowers(): Flower[] {   //without observable   (Synchornous operation)
    return FLOWERS;
  } */

  getFlowers(): Observable<Flower[]> {
    return this.http.get<Flower[]>(this.flowersUrl).pipe(
      tap(flowers => this.log('fetched flowers')),
      catchError(this.handleError('getFlowers', []))
    );
  }

  /** GET flower by id. Return `undefined` when id not found */
  getFlowerNo404<Data>(id: number): Observable<Flower> {
    const url = `${this.flowersUrl}/?id=${id}`;
    return this.http.get<Flower[]>(url).pipe(
      map(flowers => flowers[0]), // returns a {0|1} element array
      tap(h => {
        const outcome = h ? `fetched` : `did not find`;
        this.log(`${outcome} flower id=${id}`);
      }),
      catchError(this.handleError<Flower>(`getFlower id=${id}`))
    );
  }

  getFlower(id: number): Observable<Flower> {
    const url = `${this.flowersUrl}/${id}`;
    return this.http.get<Flower>(url).pipe(
      tap(_ => this.log(`fetched flower id=${id}`)),
      catchError(this.handleError<Flower>(`getFlower id=${id}`))
    );
  }

  updateFlower(flower: Flower): Observable<any> {
    return this.http.put(this.flowersUrl, flower, httpOptions).pipe(
      tap(_ => this.log(`updated flower id=${flower.id}`)),
      catchError(this.handleError<any>('updateFlower'))
    );
  }

  addFlower(flower: Flower): Observable<Flower> {
    return this.http.post<Flower>(this.flowersUrl, flower, httpOptions).pipe(
      tap((flower: Flower) => this.log(`added flower w/ id=${flower.id}`)),
      catchError(this.handleError<Flower>('addFlower'))
    );
  }

  deleteFlower(flower: Flower | number): Observable<Flower> {
    const id = typeof flower === 'number' ? flower : flower.id;
    const url = `${this.flowersUrl}/${id}`;

    return this.http.delete<Flower>(url, httpOptions).pipe(
      tap(_ => this.log(`deleted flower id=${id}`)),
      catchError(this.handleError<Flower>('deleteFlower'))
    );
  }
  searchFlowers(term: string): Observable<Flower[]> {
    if (!term.trim()) {
      // if not search term, return empty flower array.
      return of([]);
    }
    return this.http
      .get<Flower[]>(`${this.flowersUrl}/?name=${term}`)
      .pipe
      //tap(_ => this.log(`found flowers matching "${term}"`)),
      //catchError(this.handleError<Flower[]>('searchFlowers', []))
      ();
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); // log to console instead
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

  private log(message: string) {
    this.flowerMessageService.add(`FlowerService: ${message}`);
  }
}
