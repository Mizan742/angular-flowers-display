import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {HttpClientInMemoryWebApiModule} from 'angular-in-memory-web-api';
import {InMemoryDataService} from './in-memory-data.service';

import {AppComponent} from './app.component';
import {FlowersComponent} from './flowers/flowers.component';
import {FlowerDetailComponent} from './flower-detail/flower-detail.component';
import {FlowerMessagesComponent} from './flower-messages/flower-messages.component';
import {AppRoutingModule} from './app-routing.module';
import {DashboardComponent} from './dashboard/dashboard.component';
import { FlowerSearchComponent } from './flower-search/flower-search.component';

@NgModule({
  declarations: [
    AppComponent,
    FlowersComponent,
    FlowerDetailComponent,
    FlowerMessagesComponent,
    DashboardComponent,
    FlowerSearchComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(InMemoryDataService, {
      dataEncapsulation: false,
    }),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
