import {Component, OnInit} from '@angular/core';
import {FlowerService} from '../flower.service';
import {Flower} from '../flower';

// import {FLOWERS} from '../mock-flowers';
// import {Flower} from '../flower';

@Component({
  selector: 'app-flowers',
  templateUrl: './flowers.component.html',
  styleUrls: ['./flowers.component.css'],
})
export class FlowersComponent implements OnInit {
  flowers: Flower[];

  constructor(private flowerService: FlowerService) {}

  ngOnInit() {
    this.getFlowers();
  }

  add(name: string): void {
    name = name.trim();
    if (!name) {
      return;
    }
    this.flowerService.addFlower({name} as Flower).subscribe(flower => {
      this.flowers.push(flower);
    });
  }

  getFlowers(): void {
    this.flowerService
      .getFlowers()
      .subscribe(flowers => (this.flowers = flowers));
  }

  delete(flower: Flower): void {
    this.flowers = this.flowers.filter(h => h !== flower);
    this.flowerService.deleteFlower(flower).subscribe();
  }
}
