import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FlowersComponent} from './flowers/flowers.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {FlowerDetailComponent} from './flower-detail/flower-detail.component';

const routes: Routes = [
  {path: '', redirectTo: '/dashboard', pathMatch: 'full'},
  {path: 'dashboard', component: DashboardComponent},
  {path: 'detail/:id', component: FlowerDetailComponent},
  {path: 'flowers', component: FlowersComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: [],
})
export class AppRoutingModule {}
