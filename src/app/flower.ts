export class Flower {
  id: number;
  name: string;
  url: string;
}
