import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class FlowerMessageService {
  flowerMessages: string[] = [];
  constructor() {}
  add(flowerMessage: string) {
    this.flowerMessages.push(flowerMessage);
  }

  clear() {
    this.flowerMessages = [];
  }
}
