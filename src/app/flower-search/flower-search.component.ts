import {Component, OnInit} from '@angular/core';

import {Observable, Subject} from 'rxjs';

import {debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';

import {Flower} from '../flower';
import {FlowerService} from '../flower.service';

@Component({
  selector: 'app-flower-search',
  templateUrl: './flower-search.component.html',
  styleUrls: ['./flower-search.component.css'],
})
export class FlowerSearchComponent implements OnInit {
  flowers$: Observable<Flower[]>;
  private searchTerms = new Subject<string>();

  constructor(private flowerService: FlowerService) {}

  // Push a search term into the observable stream.
  search(term: string): void {
    this.searchTerms.next(term);
  }

  ngOnInit(): void {
    this.flowers$ = this.searchTerms.pipe(
      // wait 300ms after each keystroke before considering the term
      debounceTime(300),

      // ignore new term if same as previous term
      distinctUntilChanged(),

      // switch to new search observable each time the term changes
      switchMap((term: string) => this.flowerService.searchFlowers(term))
    );
  }
}
