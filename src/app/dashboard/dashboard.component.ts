import {Component, OnInit} from '@angular/core';
import {Flower} from '../flower';
import {FlowerService} from '../flower.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  flowers: Flower[] = [];

  constructor(private flowerService: FlowerService) {}

  ngOnInit() {
    this.getFlowers();
  }

  getFlowers(): void {
    this.flowerService
      .getFlowers()
      .subscribe(flowers => (this.flowers = flowers.slice(1, 6)));
  }
}
